WTF_CSRF_ENABLED = True

#WTF requires user to use a secret key
SECRET_KEY = 'a-very-secret-secret'

import os


basedir = os.path.abspath(os.path.dirname(__file__))

#use of SQL ALCHEMY to create our database
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'UserDetails.db')

SQLALCHEMY_BINDS = {
    'shoppingitems':        'sqlite:///' + os.path.join(basedir, 'ShoppingItems.db'),
    'shoppingcart':        'sqlite:///' + os.path.join(basedir, 'ShoppingCarts.db'),
   
}
SQLALCHEMY_TRACK_MODIFICATIONS = True    