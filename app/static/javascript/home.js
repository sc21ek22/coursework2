

// used to alert user to log in if an add to cart request is made
$(document).ready(function(){
  $(".invalidbutton").click(function(){
    alert("Please Log In");
  });
});


// used to delete an item from cart
$(document).ready(function() {
  $('#deletebutton').on('click', function() {
      $.ajax({
          url: "{{ url_for('deleteitem') }}",
          method: "POST",
          data: JSON.stringify($(this).attr("value")),
          contentType: 'application/json;charset=UTF-8',
          success: function(data) {
              console.log(data);
          }
      });
  });
})



// used to display reset password form upon a reset password request
$(document).ready(function() {
  $('#resetpassword').on('click', function() {

    document.getElementById('passwordpassword').style.display = "block";

  });
})

