
#python file that includes template of database used in website

from app import db

#define the UserDetails database as a class
class UserDetails(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40))
    surname = db.Column(db.String(40))
    email = db.Column(db.String(50))
    password = db.Column(db.String(20))
    #Initialize database relationship
    cart = db.relationship('ShoppingCarts', backref='User')

    #define every entry of database as self.
    def __init__(self,name,surname,email,password):
        self.email = email;
        self.name = name;
        self.surname = surname;
        self.password = password;

#define the ShoppingItems database as a class
class ShoppingItems(db.Model):   
    __bindkey__ = "shoppingitems"
    __tablename__= "shoppingitems"
    id = db.Column(db.Integer,primary_key=True)
    ptype = db.Column(db.Integer)
    name = db.Column(db.String(64))
    collection = db.Column(db.Boolean, default="false")
    description = db.Column(db.Text)
    price = db.Column(db.Float)
    image = db.Column(db.String(64))
    #Initialize database relationship
    cart = db.relationship('ShoppingCarts', backref='Product')

     #define every entry of database as self.
    def __repr__(self):
        return '{}{}{}{}{}{}{}'.format(self.id, self.ptype, self.name, self.collection, self.description, self.price,self.image)

#define the ShoppingCarts database as a class
class ShoppingCarts(db.Model):   
    __bindkey__ = "shoppingcart"
    __tablename__= "shoppingcart"
    id = db.Column(db.Integer,primary_key=True)
     #Set database relationship between cart and product, productID is used as a foreign key
    productid = db.Column(db.Integer, db.ForeignKey('shoppingitems.id'))
     #Set database relationship between cart and user, userID is used as a foreign key
    userid = db.Column(db.Integer,db.ForeignKey('user.id'))
    quantity = db.Column(db.Integer)
    totalprice = db.Column(db.Float,default=0)

     #define every entry of database as self.
    def __repr__(self):
        return '{}{}{}{}{}'.format(self.id, self.productid, self.userid,  self.quantity,self.totalprice)

