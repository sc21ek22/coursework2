from flask import render_template, flash, request, redirect, url_for, redirect, request, session
from app import app,db
from datetime import datetime
from .forms import SignUp, SignIn, PasswordReset
from werkzeug.security import generate_password_hash, check_password_hash
from .models import UserDetails, ShoppingItems, ShoppingCarts
from flask_login import login_user

#Create all databases
db.create_all()

#route for home page
@app.route('/', methods=['GET','POST'])
def home():
    #initialize forms to be displayed in home page
    form = SignUp()
    signinform = SignIn()
    passwordform = PasswordReset()
    return render_template("base.html", form=form,signinform=signinform,passwordform=passwordform) 
#route for user sign up
@app.route('/signup', methods=['GET','POST'])
def signup(): 
    #initialize forms to be displayed in home page
    form = SignUp()
    signinform = SignIn()
    passwordform = PasswordReset()
    #check if form submission is valid
    if form.validate_on_submit():
        #get value of email
        flash("test")
        email = request.form['email']
  
        user = UserDetails.query.filter_by(email=email).first()
        if user:
            flash("Please use a different email")
        else:
           
            #get values of all entries filled in form and create a new UserDetails entity with those values
            name = request.form['name']
            surname = request.form['surname']
            password = request.form['password']
            new = UserDetails(name=name,surname=surname,email=email,password=generate_password_hash(password, method='sha256'))
            #add and commit this new user to the database, the details are now logged into the database for future log ins.
            db.session.add(new)
            db.session.commit()
            return redirect('/')
    return render_template("base.html", form=form,signinform=signinform, passwordform=passwordform) 

 #route for sign in   
@app.route('/signin', methods=['GET','POST'])
def signin():
    #initialize forms to be displayed in home page
    form = SignUp()
    signinform = SignIn()
    passwordform = PasswordReset()
    #check if form submission is valid
    if signinform.validate_on_submit():
        #find entity with email entered in form
        user = UserDetails.query.filter_by(email=signinform.email.data).first()
        #check if there is a user with that email and check if the entered password is the same as the password in the database
        if user:
            if check_password_hash(user.password,signinform.password.data):
                #create a session with the user details, user is now logged in and can add items to their cart
                session["name"] = user.name
                session["email"] = user.email
           
    return render_template("base.html", form=form,signinform=signinform, passwordform=passwordform) 

@app.route('/passwordreset', methods=['GET','POST'])
def passwordreset():
    #initialize forms to be displayed in home page
    form = SignUp()
    signinform = SignIn()
    passwordform = PasswordReset()
    #check if form submission is valid
    if passwordform.validate_on_submit():
        #check if there is a user with that email 
        user = UserDetails.query.filter_by(email=signinform.email.data).first()
        #check if the two passwords entered are the same
        if user and passwordform.password1.data == passwordform.password2.data:
            #create a hash password, update the database and commit.
            user.password = generate_password_hash(passwordform.password1.data, method='sha256')
            db.session.commit()
    return render_template("base.html", form=form,signinform=signinform, passwordform=passwordform) 

#route for logging out
@app.route('/logout', methods=['GET','POST'])
def logout():
    #set the session name and email to None. now there is no active session
    session["name"] = None
    session["email"] = None
    return redirect("/")
    

#route for displaying items and adding to cart
@app.route('/items', methods=['GET','POST'])
def items():
    #check if there is a post request
    if request.method == 'POST':
        #using ajax, determine which item was requested to be added to cart by its product id
        code = request.form['productid']
        #check if there is an active session before trying to add to cart
        email = session["email"]
        if email:
            #find product with the product id posted through ajax
            itemtocart = ShoppingItems.query.filter_by(id=code).first()
            #find the userid of the user in current session by their email
            sessionid = UserDetails.query.filter_by(email=email).first()
            #check if the item is already in the users cart, if it is, increment the quantity by one.
            checkifincart = ShoppingCarts.query.filter_by(productid=code, userid=sessionid.id).first()
            if checkifincart:
                checkifincart.quantity = checkifincart.quantity + 1
                db.session.commit()
            else:
                #otherwise add the product to the database
                p = ShoppingCarts(productid=code, userid=sessionid.id,quantity=1,totalprice=itemtocart.price)
                db.session.add(p)
                db.session.commit()
    #using queries seperate the products in terms of their product type to be displayed seperately
    return render_template("items.html",values=ShoppingItems.query.filter_by(collection=True).all(), necklace=ShoppingItems.query.filter_by(ptype=1).all(), bracelet=ShoppingItems.query.filter_by(ptype=2).all(), ring=ShoppingItems.query.filter_by(ptype=3).all()) 

#route for user home page
@app.route('/user', methods=['GET','POST'])
def user():
    #find the user id of current session and display their cart
    email = session["email"]
    sessionid = UserDetails.query.filter_by(email=email).first()
    #used inner join to show items in cart where user id = session id
    return render_template("user.html", values=ShoppingCarts.query.join(ShoppingItems, ShoppingItems.id==ShoppingCarts.productid).join(UserDetails, UserDetails.id == ShoppingCarts.userid).add_columns(ShoppingItems.name, ShoppingItems.image, ShoppingItems.price, ShoppingCarts.quantity, ShoppingItems.id))

#route for deleting an item from cart
@app.route('/deleteitem', methods=['GET','POST'])
def deleteitem():
    email = session["email"]
    #check which product was requested to be deleted by its product id
    code = request.data
    if email:
        itemtocart = ShoppingItems.query.filter_by(id=code).first()
        sessionid = UserDetails.query.filter_by(email=email).first()
        checkifincart = ShoppingCarts.query.filter_by(userid=sessionid.id).first()
        #if item is in cart, delete it.
        if checkifincart:
            db.session.delete(checkifincart)
            db.session.commit()
            return redirect("/user")
        #used inner join to show items in cart where user id = session id
        val = ShoppingCarts.query.join(ShoppingItems, ShoppingItems.id==ShoppingCarts.productid).join(UserDetails, UserDetails.id == ShoppingCarts.userid).add_columns(ShoppingItems.name, ShoppingItems.image, ShoppingItems.price, ShoppingCarts.quantity, ShoppingItems.id)
    return render_template("items.html", values= val)
