#python file that includes template of forms used in website

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, EmailField
from wtforms.validators import InputRequired, Email, Length


from wtforms import TextAreaField, BooleanField, DateField
from wtforms.validators import DataRequired
from datetime import datetime

#sign up form
class SignUp(FlaskForm):
    name = StringField('name', validators=[InputRequired(), Length(max=40)])
    surname = StringField('surname', validators=[InputRequired(), Length(max=40)])
    email = EmailField('email', validators=[InputRequired(), Email(), Length(max=50)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=20)])
#sign in form
class SignIn(FlaskForm):
    email = EmailField('email', validators=[InputRequired(), Email(), Length(max=50)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=20)])
    remember = BooleanField('remember me')

#password reset form
class PasswordReset(FlaskForm):
    email = EmailField('email', validators=[InputRequired(), Email(), Length(max=50)])
    password1 = PasswordField('password1', validators=[InputRequired(), Length(min=8, max=20)])
    password2 = PasswordField('password2', validators=[InputRequired(), Length(min=8, max=20)])

